import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Observable, of, Subscription } from 'rxjs';

import { ApiConfService } from './app-api/api-conf/api-conf.service';
import { NewsType, Word } from './app-api/api-conf/api-conf.interfaces';

import { Store } from '@ngrx/store'
import { ChosenWordState } from './app-api/api-store/store.interfaces';
import { NewWord } from './app-api/api-store/actions/choose-word.actions';
import { selectWord } from './app-api/api-store/store.selectors';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	data$: Observable<NewsType> = of();
	word$: Observable<any> | undefined;

	nClick: number = 0;
	nLetters: number = 0;
	
	nClickLabel: string | undefined
	guess: string = '';
	isDisabledClick = false;
	isDisabledNewWord = false;

	@ViewChild('showWordByWord', {static: false, read: ElementRef}) buttonShow!: ElementRef;

	title = 'ng11-words';

	constructor(
		private api: ApiConfService,
		private _store: Store<ChosenWordState>,
		
		private ngRendering: Renderer2,
	) {	}


	ngOnInit() {
		this.data$ = this.api.getNews();
		// this.word$ = this.api.getWord();
		
		this._store.dispatch(new NewWord());
		this.word$ = this._store.select(selectWord);
	}

	clickButton(): void {
		// console.log("Hello ", this.buttonShow.nativeElement);
		
		this.nClick ++;
		
		this._store.select(selectWord).subscribe( word => {

			const aLettersWord = word.payload.p.split('');
			
			if(this.nClick <= aLettersWord.length) {
				this.guess += aLettersWord[this.nClick - 1];
			} else {
				// this.ngRendering.addClass(this.buttonShow.nativeElement, 'xxxxxx');
				// this.ngRendering.setStyle(this.buttonShow.nativeElement, 'position', 'absolute');

				this.guess = '';
				this.nClick = 0;
			}
		});
	}

	initialize(): void {
		const ask = "Sure...?";
		const confirm = window.confirm(ask);
		
		if(confirm) {
			this._store.dispatch(new NewWord());
			this._store.select(selectWord).subscribe( word => {
				this.guess = '';
				this.nClick = 0;
				
				// console.log(word.length, this.guess.length)
			});
		}
	}
}
