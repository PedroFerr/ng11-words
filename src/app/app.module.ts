import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// .... and some fancy options:
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { environment } from 'src/environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularMaterialModule } from './angular-material.module'
import { ApiModulesModule } from './app-api/api-modules.module';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		BrowserAnimationsModule,


		StoreModule.forRoot({}),
        EffectsModule.forRoot([]),

        // --------------------------
        StoreDevtoolsModule.instrument({
			maxAge: 15,						// Retains last 15 states
			logOnly: environment.production // Restrict extension to log-only mode
		}),
        // Stuff Angular's routes into the App's Store:
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
		// --------------------------
        
		AngularMaterialModule,
		ApiModulesModule,

		AppRoutingModule,
	],
	providers: [],
	bootstrap: [AppComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule { }
