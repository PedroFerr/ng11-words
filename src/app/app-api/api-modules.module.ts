import { NgModule } from '@angular/core';

// REDUX affairs:
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// ... and to use the redux-store of this Main App Modules 'app-modules':
import { reducerName, reducers } from './api-store/store.reducers';
import { ChooseWordEffects } from './api-store/effects/choose-word.effects';

// This 'api-modules' Module to export is needed ONLY, so far, to inject the redux-store of this 'app-api'.
// We could done it - that was the first approach - directly from there, using:
//     StoreModule.forRoot(reducers),
//     EffectsModule.forRoot([ChooseWordEffects]),
// but then the Store for the 'router', somehow, could not be inserted into any kind of Object ('appModules', more precisely)
// (it just wasn't triggered, on any Angular's Router changes...)
// So... we did it this way - inject it like we do on any Features Modules - only that this belongs to the Ng8 SAAS Platform (the App) exclusively.
//
// Nothing fancy - just a better and consistent organization of the Redux Store selectors.
@NgModule({
    declarations: [ ],
    imports: [
        // ... forRoot() is only @ app.module.ts -> it's the bootstrap component; all the others are 'forFeature()'
        StoreModule.forFeature(reducerName, reducers),
        EffectsModule.forFeature([ChooseWordEffects]),
    ],
    providers: [ ]
})
export class ApiModulesModule { }
