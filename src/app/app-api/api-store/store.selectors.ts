import { createSelector, createFeatureSelector } from '@ngrx/store';

import { ChosenWordState } from './store.interfaces';
import { reducerName } from './store.reducers';

const getAppModulesState = createFeatureSelector<ChosenWordState>(reducerName);

// Redux Store SELECTORS, for the main App's 'app-modules' redux-store:
export const selectWord = createSelector( getAppModulesState, (state: ChosenWordState) => state.word);
