import { ECheckActions, CheckActions } from '../actions/choose-word.actions';
import { ChosenWordState, initialChosenWordState } from '../store.interfaces';

export const chosenWordReducer = (state = initialChosenWordState, action: CheckActions): ChosenWordState => {
    switch (action.type) {
        case ECheckActions.NewWordSuccess: {
            // return {
            //     ...state,
            //     word: action.payload
            // };

            // return action.payload;

            // return Object.assign({}, state, {
            //     word: action.payload,
            //     length: action.payload.length
            // });

            return Object.assign({}, state, action.payload);

        }

        default:
            return state;
    }
};
