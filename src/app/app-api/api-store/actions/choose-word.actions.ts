import { Action } from '@ngrx/store';
import { Word } from '../../api-conf/api-conf.interfaces';

export enum ECheckActions {
    NewWord = '[Word] New word...',
    NewWordSuccess = '[Word] User has a NEW word!',
    // Finally, but not the least:
    CheckFailed = '[Word ERROR] Couldn\'t check new word... check console.logs!'
}

export class NewWord implements Action {
    public readonly type = ECheckActions.NewWord;
}
export class NewWordSuccess implements Action {
    public readonly type = ECheckActions.NewWordSuccess;
    constructor(public payload: Word) { }
}


// Finally, but not the least:
export class CheckFailed implements Action {
    public readonly type = ECheckActions.CheckFailed;
    constructor(public payload?: any) { }
}

export type CheckActions = NewWord | NewWordSuccess
    // Finally, but not the least:
    | CheckFailed
;