import { ActionReducerMap } from '@ngrx/store';

import { ChosenWordState } from './store.interfaces';

import { chosenWordReducer } from './reducers/choose-word.reducers';

export const reducerName = 'chosen word';
export const reducers: ActionReducerMap<ChosenWordState, any> = {
    word: chosenWordReducer
};
