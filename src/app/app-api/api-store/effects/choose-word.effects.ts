import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store'

import { Observable, of } from 'rxjs';
import { switchMap, map, withLatestFrom, tap, finalize, catchError } from 'rxjs/operators';

// Services
import { ApiConfService } from '../../api-conf/api-conf.service';
import { ChosenWordState } from '../store.interfaces';

// Actions, Interfaces and Selectors
import { ECheckActions, NewWord, NewWordSuccess,  CheckFailed } from '../actions/choose-word.actions';
import { selectWord  } from '../store.selectors';

import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class ChooseWordEffects {

    @Effect()
    newWord$ = this._actions$.pipe(
        ofType<NewWord>(ECheckActions.NewWord),
        withLatestFrom(this._store.select(selectWord)),

        switchMap(
          () => this._authService.getWord()
            .pipe(
                map(
                    (payload: any) => {
                        return new NewWordSuccess({
                            payload: payload,
                            length: payload.s.length
                        })
                    }
                    // (payload: any) => new NewWordSuccess(payload)
                ),
                // Say you want to trgigger some Action, after Effect has finalize the Service request and got a response:
                // finalize(() => this._store.dispatch(new loadingActions.TurnLoadingOff())),

                // Coming to the end, catch any (Http) possible error and trigger ERROR Action:
                catchError((error: HttpErrorResponse) => of(new CheckFailed(error)))
            )
        )
    );

    // =============================================

    // Finally, but not the least:
    @Effect({ dispatch: false })
    CheckFailed$: Observable<CheckFailed> = this._actions$.pipe(
        ofType<CheckFailed>(ECheckActions.CheckFailed),
        tap((payload: CheckFailed) => {
            // You could here trigger some Actions, that could trigger other Effects/Services, and so on:
            // this._store.dispatch(new loadingActions.TurnLoadingOff());
            // this._store.dispatch(new errorActions.ErrorLayoutShow(payload));
            console.error('Redux Store ERROR for "CheckFailed$" Effect:', payload);
        })
    );

    constructor(
        private _authService: ApiConfService,
        private _actions$: Actions,
        private _store: Store<ChosenWordState>
    ) { }
}