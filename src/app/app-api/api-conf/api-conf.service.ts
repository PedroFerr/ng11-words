import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { map, catchError, shareReplay } from 'rxjs/operators';
import { Observable, pipe, throwError } from 'rxjs';

import { NewsType } from './api-conf.interfaces';

@Injectable({
	providedIn: 'root'
})
export class ApiConfService {

	constructor(private http: HttpClient) { }

	getWord(): Observable<any> {
		const endUri = `http://127.0.0.1:4100/`
		return this.http.get<any>(endUri)
			.pipe(
				map(
					(res: HttpResponse<any>) => res,
					catchError((error: HttpErrorResponse) => throwError(error))
				)
				// Faz TODA a diferença, para não passar Observable's...
				, shareReplay(1)
			)
	}

	getNews(): Observable<NewsType> {
		const apiKey = 'db81900babba4b6aafa79015e2e14183';
		const endUri = `https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=${apiKey}`;

		// this.http.get<NewsType>(endUri).subscribe(entulho => {console.log(entulho)});

		return this.http.get<NewsType>(endUri);

	}

}
