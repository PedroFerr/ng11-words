export type NewsType = {
	status: string;
	totalResults: number;
	articles: Array<{
		author: string;
		content: string;
		description: string;
		publishedAt: string;
		source: { 
			id: string;
			name: string;
		}
		title: string;
		url: string;
		urlToImage: string;
	}>
};

// export type Word = {
// 	p: string;
// 	s: string;
// }
export type Word = any
// export type Word = {
// 	payload: string,
// 	length: number
// }

