import word from './words'

function shuffelWord(word) {
    var shuffledWord = '';
    word = word.split('');
    while (word.length > 0) {
        shuffledWord += word.splice(Math.random() * word.length << 0, 1);
    }
    return shuffledWord;
}
const shuffle = shuffelWord(word);

export default shuffle;
