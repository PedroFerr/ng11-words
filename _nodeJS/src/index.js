const hostname = '127.0.0.1';
const port = '4100';

import express from 'express';
const app = express();

// import word from './words';
import words from './words';

// import shuffle from './shuffles';

function palavra(nLength) {
	return words[Math.floor(Math.random() * words.length)];
}

function shuffelWord(word) {
	var shuffledWord = '';
	word = word.split('');
	while (word.length > 0) {
		shuffledWord += word.splice(Math.random() * word.length << 0, 1);
	}
	return shuffledWord;
}

// Enable CORS:
app.use((req, resp, next) => {
	resp.header("Access-Control-Allow-Origin", "*");
	resp.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

app.get('/', (req, res) => {

	const word = palavra(2);
	const shuffle = shuffelWord(word);

	const aLettersWord = word.split('');
	let guess = '';
	for (let s = 0; s < aLettersWord.length; s++) {
		guess += aLettersWord[s];
	}

	console.log(word, shuffle, guess);

	res.statusCode = 200;
	res.setHeader('Content-Type', 'text/html');
	res.end(
		`{"p":"${word}", "s":"${shuffle}"}`
	);
	// res.end();
});


app.listen(port, hostname, () => {
	console.log(`Server running at http://${hostname}:${port}/`)
});
